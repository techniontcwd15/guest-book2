const fs = require('fs');
const db = [];
let currentId = 0;

module.exports = {
  getAll() {
    return new Promise((resolve, reject) => {
      fs.readFile('./db.json', 'utf-8', (err, db ) => {
        if(err) {
          return reject(err);
        }

        resolve(JSON.parse(db));
      });
  });
},

  add(message) {
    return this.getAll()
    .then((db) => {
      return new Promise((resolve, reject) => {
        db.push({id: currentId++, ...message});

        fs.writeFile('./db.json', JSON.stringify(db),(err) => {
          if(err) {
            return reject(err);
          }

          resolve();
        })
      })
    });
  },

  edit(id, newDetails) {
    for (var i = 0; i < db.length; i++) {
      if(db[i].id === id) {
        db[i] = {...db[i], ...newDetails};
        return;
      }
    }
  },

  remove(id) {
    for (var i = 0; i < db.length; i++) {
      if(db[i].id === id) {
        db.splice(db.indexOf(db[i]), 1);
      }
    }
  }
}
