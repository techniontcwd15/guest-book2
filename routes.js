const express = require('express');
const Message = require('./model/message');
const router = express.Router();

// get all messages
router.get('/', (req, res) => {
  Message.getAll().then((db) => {
    res.json(db);
  })
});

// add message
router.post('/', (req, res) => {
  Message.add(req.body).then(() => {
    res.status(204).end();
  });
});

// edit message
router.put('/:id', (req, res) => {
  const id = parseInt(req.params.id);
  Message.edit(id, req.body);
  res.status(204).end();
});

// remove message
router.delete('/:id', (req, res) => {
  const id = parseInt(req.params.id);
  Message.remove(id);
  res.status(204).end();
});

module.exports = router;
