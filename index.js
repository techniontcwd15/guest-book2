const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes');
// create the server
const app = express();

// get all the files from angular build
// this js is the client js NOT the nodejs (server)
// index.html would be the entry point
app.use(express.static('guestbook-client/dist'));
// middleware, define body in request.
// will parse JSON in requests
app.use(bodyParser.json());

// define `messages` routes
app.use('/api/messages', routes);

app.listen(8080, () => {
  console.log('server listening on port 8080');
})
