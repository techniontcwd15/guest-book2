import { Component } from '@angular/core';
import { MessageService } from './message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MessageService]
})
export class AppComponent {
  messages = [];

  constructor(private messageService:MessageService) {
    this.getMessages();
  }

  getMessages() {
    this.messageService.getAll().subscribe((allMessages) => {
      this.messages = allMessages;
    });
  }

  deleteMessage(id) {
    this.messageService.deleteMessage(id)
      .subscribe(() => {
        this.getMessages();
      });
  }

  editMessage(id, title, body) {
    this.messageService.editMessage(id,title,body)
    .subscribe(() => {
      this.getMessages();
    });
  }

  addMessage(title, body) {
    this.messageService.addMessge(title, body).subscribe(()=>{
      this.getMessages();
    });
  }
}
