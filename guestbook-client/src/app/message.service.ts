import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MessageService {

  constructor(private http: Http) { }

  getAll() {
    return this.http.get('/api/messages')
           .map(res=>res.json());
  }

  deleteMessage(id) {
    return this.http.delete('/api/messages/' + id);
  }

  editMessage(id, title, body) {
    return this.http.put('/api/messages/' + id, {title, body});
  }

  addMessge(title, body) {
    return this.http.post('/api/messages', { title, body});
  }

}
